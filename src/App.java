import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        // déclarations des tableaux pour stocker les informations des employés
        String[] employeeNames = { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
        int[] hoursWorked = { 35, 38, 35, 38, 40 };
        double[] hourlyRates = { 12.5, 15.0, 13.5, 14.5, 13.0 };
        String[] positions = { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };

        // itération dans le tableau
        for (int i = 0; i < employeeNames.length; i++) {
            // on initialise le salaire à 0 pour chaque employé dans une variable
            double salaire = 0;

            // Calculer le salaire de base
            salaire = hoursWorked[i] * hourlyRates[i];

            // Calculer les heures supplémentaires si nécessaire
            if (hoursWorked[i] > 35) {
                int overtimeHours = hoursWorked[i] - 35;
                salaire += overtimeHours * (hourlyRates[i] * 1.5);
            }

            // Afficher le salaire de l'employé
            System.out.println(
                    employeeNames[i] + " - Poste: " + positions[i] + " - Salaire hebdomadaire: " + salaire + " euro");
        }
        // Déclaration d'une chaîne de caractères pour la position recherchée
        String searchPosition = "Caissier";

        // Création d'une liste pour stocker les noms des employés correspondant à la
        // position recherchée
        ArrayList<String> name = new ArrayList<>();

        // boucle pour parcourir (itérer) dans le tableau employés
        for (int i = 0; i < employeeNames.length; i++) {

            // Vérification si la position actuelle correspond à la position recherchée
            if (searchPosition == positions[i]) {
                // Ajout du nom de l'employé à la liste si la position correspond
                name.add(employeeNames[i]);
            }
        }
        // affiche le nom et le metier de l'employé
        System.out.println(name + " - Poste:" + searchPosition);
        
        //si le nom est vide (qu'il n'a pas été trouvé)
        if (name.isEmpty()) {
            //affiche la phrase 
            System.out.println("Aucun employé trouvé.");
        }

    }
}
